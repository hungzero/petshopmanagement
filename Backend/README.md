# Monkey Market

Monkey Market Backend API

1. Git Clone

2. Download & Install Docker: https://www.docker.com/get-started

3. Run START_DEV.bat

4. Endpoints:
    - APIs: localhost:8000
    - APIs documents: localhost:8000/docs
    - MySQL: localhost:32000
    - PhpMyAdmin: localhost:8080