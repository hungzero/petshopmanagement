from fastapi import APIRouter

from app.api.api_v1.endpoints import  items, login, users, utils 
from app.api.api_v1.endpoints.default import tests, Pet, PetService, Picture, Service
from app.api.api_v1 import api_ext

api_router = APIRouter()

api_router.include_router(login.router, tags=["login"])
api_router.include_router(tests.router, prefix="/tests", tags=["tests"])
api_router.include_router(users.router, prefix="/users", tags=["users"])
api_router.include_router(utils.router, prefix="/utils", tags=["utils"])
api_router.include_router(items.router, prefix="/items", tags=["items"])
api_router.include_router(api_ext.api_router_ex)
api_router.include_router(Pet.router, prefix="/Pet", tags=["Pet"])
api_router.include_router(PetService.router, prefix="/PetService", tags=["PetService"])
api_router.include_router(Picture.router, prefix="/Picture", tags=["Picture"])
api_router.include_router(Service.router, prefix="/Service", tags=["Service"])
