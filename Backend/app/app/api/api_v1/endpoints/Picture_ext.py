# Python class represent the api endpoint
import io
from typing import List
from uuid import uuid4, UUID
from datetime import datetime

from starlette.responses import Response, StreamingResponse
from fastapi import APIRouter, Depends,File, UploadFile, HTTPException
from sqlalchemy.orm import Session

from app.core import config
from app.crud.default import Picture_crud
from app.api.utils.db import get_db
from app.api.utils.security import get_current_active_user
from app.db_models.user import User as DBUser
from app.db_models.Picture import Picture as DBPicture
from app.models.default.Picture import Picture, PictureCreate, PictureUpdate

from app.crud import binary_data_crud
from app.db_models.BinaryData import BinaryData as DBBinaryData
from app.models.binary_data import BinaryDataCreate, BinaryData

router = APIRouter()

@router.get("/Storage/{uuid}")
def read_uploaded_picture(
    *,
    db: Session = Depends(get_db),
    uuid: str
):
    """
    Get uploaded picture data by uuid.
    """
    DBBinaryData = binary_data_crud.get_by_uuid(db_session=db, uuid=uuid)
    if not DBBinaryData:
        raise HTTPException(status_code=400, detail="Picture data not found")

    headers= {
        "x-size": DBBinaryData.file_size if DBBinaryData.file_size else "",
        "x-name": DBBinaryData.file_name if DBBinaryData.file_name else "",
        "x-description": DBBinaryData.description if DBBinaryData.description else "",
        "x-timestamp": str(DBBinaryData.time_stamp) if DBBinaryData.time_stamp else ""
    }
    
    return StreamingResponse(io.BytesIO(DBBinaryData.bin_data), media_type= DBBinaryData.file_type, headers= headers)


@router.post("/Storage", response_model=Picture)
async def create_upload_picture(
    response: Response,
    db: Session = Depends(get_db),
    picture: UploadFile = File(...),
    current_user: DBUser = Depends(get_current_active_user)):
    """
    Upload picture data.
    """
    max_file_size = (2**22)
    allow_picture_type = ["image/jpeg","image/png","image/gif"]

    if not picture.content_type in allow_picture_type:
        raise HTTPException(status_code=400, detail="File type not support, only support *.jpg, *.png , *.gif")

    content = picture.file.read()
    file_size = len(content)

    if file_size >= max_file_size:
        raise HTTPException(status_code=400, detail="File too big, only support file < 4MB")

    file_in = {}
    file_in["uuid"] = str(uuid4())
    file_in["bin_data"] = content
    file_in["file_name"] = picture.filename
    file_in["file_type"] = picture.content_type
    file_in["file_size"] = file_size
    file_in["time_stamp"] = datetime.now()

    file_created = binary_data_crud.create(db_session = db, binarydata_in = file_in, created_by = current_user.email)

    picture_in = PictureCreate(
            name = file_created.uuid,
            url = config.SERVER_HOST + config.API_V1_STR + "/Picture/Storage/" + file_created.uuid,
            type = file_created.file_type,
            seo_file_name = file_created.file_name
    )

    picture_created = Picture_crud.create(db_session=db, picture_in=picture_in, created_by=current_user.email)
    response.headers["Location"] = config.API_V1_STR + "/Picture/" + str(picture_created.id)

    return picture_created
