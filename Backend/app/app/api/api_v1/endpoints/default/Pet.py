# Python class represent the api endpoint
from typing import List

from starlette.responses import Response
from fastapi import APIRouter, Depends, HTTPException, Query
from sqlalchemy.orm import Session

from app.core import config
from app.crud.default import Pet_crud
from app.api.utils.db import get_db
from app.api.utils.security import get_current_active_user
from app.db_models.user import User as DBUser
from app.db_models.Pet import Pet as DBPet
from app.models.default.Pet import Pet, PetCreate, PetUpdate

router = APIRouter()

@router.get("/", response_model=List[Pet])
def read_pet_by_filter(
    response: Response,
    db: Session = Depends(get_db),
    skip: int = 0,
    limit: int = 300,
    load_relations: str = None,
    has_total: bool = False,
    filter: List[str] = Query(None, description="example use: ?filter=age;in;5,6,7;desc&filter=name;like;%john%;asc")):
    """
    Return filtered list based on condition.<br/>
    :param skip: skip number of record default 0<br/>
    :param limit: only take number of record<br/>
    :param load_relations: list of relation entities need to load, default None, example: country,images<br/>
    :param has_total: if true, will calculate total count and return in x-total-count response header, default false<br/>
    :param filter: apply multiple filter, example use: ?filter=age;in;5,6,7;desc&filter=name;like;%john%;asc"<br/>
    &nbsp;&nbsp; operator list:<br/>
        &nbsp;&nbsp;&nbsp;&nbsp; eq for ==<br/>
        &nbsp;&nbsp;&nbsp;&nbsp; lt for <<br/>
        &nbsp;&nbsp;&nbsp;&nbsp; ge for >=<br/>
        &nbsp;&nbsp;&nbsp;&nbsp; in for in_<br/>
        &nbsp;&nbsp;&nbsp;&nbsp; like for like<br/>
    &nbsp; value could be list or a string<br/>
    &nbsp; sort can be 'desc' or 'asc' or '' for not sort<br/>
    :return: list
    """
    try:
        result = Pet_crud.get_multi_by_filter( db, skip=skip, limit=limit, load_relations = load_relations, has_total=has_total, filter= filter)
    except ValueError as e:
        raise HTTPException(status_code=400, detail=str(e))
    
    if has_total:
        response.headers["x-total-count"] = str(result["total_count"])

    return result["results"]


@router.get("/{id}", response_model=Pet)
def read_pet_by_id(
    *,
    db: Session = Depends(get_db),
    id: int):
    """
    Get Pet by ID.
    """
    pet_result = Pet_crud.get(db_session=db, id=id)
    if not pet_result:
        raise HTTPException(status_code=400, detail="Pet not found")
    return pet_result


@router.post("/", response_model=Pet)
def create_pet(
    response: Response,
    *,
    db: Session = Depends(get_db),
    pet_in: PetCreate,
    current_user: DBUser = Depends(get_current_active_user)):
    """
    Create new Pet.
    """
    pet_created = Pet_crud.create(db_session=db, pet_in=pet_in, created_by=current_user.email)
    response.headers["Location"] = config.API_V1_STR + "/Pet/" + str(pet_created.id)
    return pet_created


@router.put("/{id}", response_model=Pet)
def update_pet(
    *,
    db: Session = Depends(get_db),
    id: int,
    pet_in: PetUpdate,
    current_user: DBUser = Depends(get_current_active_user)):
    """
    Update an Pet.
    """
    pet_old = Pet_crud.get(db_session=db, id=id)
    if not pet_old:
        raise HTTPException(status_code=404, detail="Pet not found")
    pet_updated = Pet_crud.update(db_session=db, pet=pet_old, pet_in=pet_in)
    return pet_updated




@router.delete("/pernament/{id}", response_model=int)
def delete_pet(
    *,
    db: Session = Depends(get_db),
    id: int,
    current_user: DBUser = Depends(get_current_active_user)):
    """
    pernament delete an Pet.
    """
    pet = Pet_crud.get(db_session=db, id=id)
    if not pet:
        raise HTTPException(status_code=404, detail="Pet not found")
    pet = Pet_crud.delete(db_session=db, id=id)
    return pet.id