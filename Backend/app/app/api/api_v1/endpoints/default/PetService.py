# Python class represent the api endpoint
from typing import List

from starlette.responses import Response
from fastapi import APIRouter, Depends, HTTPException, Query
from sqlalchemy.orm import Session

from app.core import config
from app.crud.default import PetService_crud
from app.api.utils.db import get_db
from app.api.utils.security import get_current_active_user
from app.db_models.user import User as DBUser
from app.db_models.PetService import PetService as DBPetService
from app.models.default.PetService import PetService, PetServiceCreate, PetServiceUpdate

router = APIRouter()

@router.get("/", response_model=List[PetService])
def read_petservice_by_filter(
    response: Response,
    db: Session = Depends(get_db),
    skip: int = 0,
    limit: int = 300,
    load_relations: str = None,
    has_total: bool = False,
    filter: List[str] = Query(None, description="example use: ?filter=age;in;5,6,7;desc&filter=name;like;%john%;asc")):
    """
    Return filtered list based on condition.<br/>
    :param skip: skip number of record default 0<br/>
    :param limit: only take number of record<br/>
    :param load_relations: list of relation entities need to load, default None, example: country,images<br/>
    :param has_total: if true, will calculate total count and return in x-total-count response header, default false<br/>
    :param filter: apply multiple filter, example use: ?filter=age;in;5,6,7;desc&filter=name;like;%john%;asc"<br/>
    &nbsp;&nbsp; operator list:<br/>
        &nbsp;&nbsp;&nbsp;&nbsp; eq for ==<br/>
        &nbsp;&nbsp;&nbsp;&nbsp; lt for <<br/>
        &nbsp;&nbsp;&nbsp;&nbsp; ge for >=<br/>
        &nbsp;&nbsp;&nbsp;&nbsp; in for in_<br/>
        &nbsp;&nbsp;&nbsp;&nbsp; like for like<br/>
    &nbsp; value could be list or a string<br/>
    &nbsp; sort can be 'desc' or 'asc' or '' for not sort<br/>
    :return: list
    """
    try:
        result = PetService_crud.get_multi_by_filter( db, skip=skip, limit=limit, load_relations = load_relations, has_total=has_total, filter= filter)
    except ValueError as e:
        raise HTTPException(status_code=400, detail=str(e))
    
    if has_total:
        response.headers["x-total-count"] = str(result["total_count"])

    return result["results"]


@router.get("/{id}", response_model=PetService)
def read_petservice_by_id(
    *,
    db: Session = Depends(get_db),
    id: int):
    """
    Get PetService by ID.
    """
    petservice_result = PetService_crud.get(db_session=db, id=id)
    if not petservice_result:
        raise HTTPException(status_code=400, detail="PetService not found")
    return petservice_result


@router.post("/", response_model=PetService)
def create_petservice(
    response: Response,
    *,
    db: Session = Depends(get_db),
    petservice_in: PetServiceCreate,
    current_user: DBUser = Depends(get_current_active_user)):
    """
    Create new PetService.
    """
    petservice_created = PetService_crud.create(db_session=db, petservice_in=petservice_in, created_by=current_user.email)
    response.headers["Location"] = config.API_V1_STR + "/PetService/" + str(petservice_created.id)
    return petservice_created


@router.put("/{id}", response_model=PetService)
def update_petservice(
    *,
    db: Session = Depends(get_db),
    id: int,
    petservice_in: PetServiceUpdate,
    current_user: DBUser = Depends(get_current_active_user)):
    """
    Update an PetService.
    """
    petservice_old = PetService_crud.get(db_session=db, id=id)
    if not petservice_old:
        raise HTTPException(status_code=404, detail="PetService not found")
    petservice_updated = PetService_crud.update(db_session=db, petservice=petservice_old, petservice_in=petservice_in)
    return petservice_updated




@router.delete("/pernament/{id}", response_model=int)
def delete_petservice(
    *,
    db: Session = Depends(get_db),
    id: int,
    current_user: DBUser = Depends(get_current_active_user)):
    """
    pernament delete an PetService.
    """
    petservice = PetService_crud.get(db_session=db, id=id)
    if not petservice:
        raise HTTPException(status_code=404, detail="PetService not found")
    petservice = PetService_crud.delete(db_session=db, id=id)
    return petservice.id