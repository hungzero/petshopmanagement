# Python class represent the api endpoint
from typing import List

from starlette.responses import Response
from fastapi import APIRouter, Depends, HTTPException, Query
from sqlalchemy.orm import Session

from app.core import config
from app.crud.default import Picture_crud
from app.api.utils.db import get_db
from app.api.utils.security import get_current_active_user
from app.db_models.user import User as DBUser
from app.db_models.Picture import Picture as DBPicture
from app.models.default.Picture import Picture, PictureCreate, PictureUpdate

router = APIRouter()

@router.get("/", response_model=List[Picture])
def read_picture_by_filter(
    response: Response,
    db: Session = Depends(get_db),
    skip: int = 0,
    limit: int = 300,
    load_relations: str = None,
    has_total: bool = False,
    filter: List[str] = Query(None, description="example use: ?filter=age;in;5,6,7;desc&filter=name;like;%john%;asc")):
    """
    Return filtered list based on condition.<br/>
    :param skip: skip number of record default 0<br/>
    :param limit: only take number of record<br/>
    :param load_relations: list of relation entities need to load, default None, example: country,images<br/>
    :param has_total: if true, will calculate total count and return in x-total-count response header, default false<br/>
    :param filter: apply multiple filter, example use: ?filter=age;in;5,6,7;desc&filter=name;like;%john%;asc"<br/>
    &nbsp;&nbsp; operator list:<br/>
        &nbsp;&nbsp;&nbsp;&nbsp; eq for ==<br/>
        &nbsp;&nbsp;&nbsp;&nbsp; lt for <<br/>
        &nbsp;&nbsp;&nbsp;&nbsp; ge for >=<br/>
        &nbsp;&nbsp;&nbsp;&nbsp; in for in_<br/>
        &nbsp;&nbsp;&nbsp;&nbsp; like for like<br/>
    &nbsp; value could be list or a string<br/>
    &nbsp; sort can be 'desc' or 'asc' or '' for not sort<br/>
    :return: list
    """
    try:
        result = Picture_crud.get_multi_by_filter( db, skip=skip, limit=limit, load_relations = load_relations, has_total=has_total, filter= filter)
    except ValueError as e:
        raise HTTPException(status_code=400, detail=str(e))
    
    if has_total:
        response.headers["x-total-count"] = str(result["total_count"])

    return result["results"]


@router.get("/{id}", response_model=Picture)
def read_picture_by_id(
    *,
    db: Session = Depends(get_db),
    id: int):
    """
    Get Picture by ID.
    """
    picture_result = Picture_crud.get(db_session=db, id=id)
    if not picture_result:
        raise HTTPException(status_code=400, detail="Picture not found")
    return picture_result


@router.post("/", response_model=Picture)
def create_picture(
    response: Response,
    *,
    db: Session = Depends(get_db),
    picture_in: PictureCreate,
    current_user: DBUser = Depends(get_current_active_user)):
    """
    Create new Picture.
    """
    picture_created = Picture_crud.create(db_session=db, picture_in=picture_in, created_by=current_user.email)
    response.headers["Location"] = config.API_V1_STR + "/Picture/" + str(picture_created.id)
    return picture_created


@router.put("/{id}", response_model=Picture)
def update_picture(
    *,
    db: Session = Depends(get_db),
    id: int,
    picture_in: PictureUpdate,
    current_user: DBUser = Depends(get_current_active_user)):
    """
    Update an Picture.
    """
    picture_old = Picture_crud.get(db_session=db, id=id)
    if not picture_old:
        raise HTTPException(status_code=404, detail="Picture not found")
    picture_updated = Picture_crud.update(db_session=db, picture=picture_old, picture_in=picture_in)
    return picture_updated


@router.delete("/{id}", response_model= int)
def delete_picture(
    *,
    db: Session = Depends(get_db),
    id: int,
    current_user: DBUser = Depends(get_current_active_user)):
    """
    Soft delete an Picture.
    """
    picture = Picture_crud.get(db_session=db, id=id)
    if not picture:
        raise HTTPException(status_code=404, detail="Picture not found")
    picture = Picture_crud.delete_soft(db_session=db, id=id)
    return picture.id


@router.delete("/pernament/{id}", response_model=int)
def delete_picture(
    *,
    db: Session = Depends(get_db),
    id: int,
    current_user: DBUser = Depends(get_current_active_user)):
    """
    pernament delete an Picture.
    """
    picture = Picture_crud.get(db_session=db, id=id)
    if not picture:
        raise HTTPException(status_code=404, detail="Picture not found")
    picture = Picture_crud.delete(db_session=db, id=id)
    return picture.id