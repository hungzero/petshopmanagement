from typing import List
from datetime import timedelta

from fastapi import APIRouter, Body, Depends, HTTPException
from fastapi.security import OAuth2PasswordRequestForm
from sqlalchemy.orm import Session

from app.api.utils.db import get_db
from app.api.utils.security import get_current_user
from app.core import config
from app.core.jwt import create_access_token
from app.core.security import get_password_hash


router = APIRouter()

@router.get("/", response_model=List[str])
def routing_test(
    skip: int = 0,
    limit: int = 100
):
    """
    Routing test....
    """

    items = ["Routing test...", "One","Two","Three","..."]

    return items