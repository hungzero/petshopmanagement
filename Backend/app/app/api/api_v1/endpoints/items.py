from typing import List

from fastapi import APIRouter, Depends, HTTPException
from sqlalchemy.orm import Session

from app.crud import item_crud, user_crud
from app.api.utils.db import get_db
from app.api.utils.security import get_current_active_user
from app.db_models.user import User as DBUser
from app.models.item import Item, ItemCreate, ItemUpdate

router = APIRouter()


@router.get("/", response_model=List[Item])
def read_items(
    db: Session = Depends(get_db),
    skip: int = 0,
    limit: int = 300,
    current_user: DBUser = Depends(get_current_active_user),
):
    """
    Retrieve items.
    """
    if user_crud.is_superuser(current_user):
        items = item_crud.get_multi(db, skip=skip, limit=limit)
    else:
        items = item_crud.get_multi_by_owner(
            db_session=db, owner_id=current_user.id, skip=skip, limit=limit
        )
    return items


@router.get("/me", response_model=List[Item])
def read_my_items(
    db: Session = Depends(get_db),
    skip: int = 0,
    limit: int = 300,
    current_user: DBUser = Depends(get_current_active_user),
):
    """
    Retrieve current login user's items.
    """
    items = item_crud.get_multi_by_owner(
        db_session=db, owner_id=current_user.id, skip=skip, limit=limit
    )

    return items


@router.post("/", response_model=Item)
def create_item(
    *,
    db: Session = Depends(get_db),
    item_in: ItemCreate,
    current_user: DBUser = Depends(get_current_active_user),
):
    """
    Create new item.
    """
    item = item_crud.create(db_session=db, item_in=item_in, owner_id=current_user.id, created_by=current_user.email)
    return item


@router.put("/{id}", response_model=Item)
def update_item(
    *,
    db: Session = Depends(get_db),
    id: int,
    item_in: ItemUpdate,
    current_user: DBUser = Depends(get_current_active_user),
):
    """
    Update an item.
    """
    item = item_crud.get(db_session=db, id=id)
    if not item:
        raise HTTPException(status_code=404, detail="Item not found")
    if not user_crud.is_superuser(current_user) and (item.owner_id != current_user.id):
        raise HTTPException(status_code=400, detail="Not enough permissions")
    item = item_crud.update(db_session=db, item=item, item_in=item_in, updated_by=current_user.email)
    return item


@router.get("/{id}", response_model=Item)
def read_user_me(
    *,
    db: Session = Depends(get_db),
    id: int,
    current_user: DBUser = Depends(get_current_active_user),
):
    """
    Get item by ID.
    """
    item = item_crud.get(db_session=db, id=id)
    if not item:
        raise HTTPException(status_code=400, detail="Item not found")
    if not user_crud.is_superuser(current_user) and (item.owner_id != current_user.id):
        raise HTTPException(status_code=400, detail="Not enough permissions")
    return item


@router.delete("/{id}", response_model=Item)
def delete_item(
    *,
    db: Session = Depends(get_db),
    id: int,
    current_user: DBUser = Depends(get_current_active_user),
):
    """
    Delete an item.
    """
    item = item_crud.get(db_session=db, id=id)
    if not item:
        raise HTTPException(status_code=404, detail="Item not found")
    if not user_crud.is_superuser(current_user) and (item.owner_id != current_user.id):
        raise HTTPException(status_code=400, detail="Not enough permissions")
    item = item_crud.delete(db_session=db, id=id)
    return item
