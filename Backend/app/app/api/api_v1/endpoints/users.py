from typing import List, Optional
from datetime import datetime

from fastapi import APIRouter, Body, Depends, HTTPException
from fastapi.encoders import jsonable_encoder
from pydantic import EmailStr
from sqlalchemy.orm import Session

from app.crud import item_crud, user_crud
from app.api.utils.db import get_db
from app.api.utils.security import get_current_active_superuser, get_current_active_user
from app.core import config
from app.db_models.user import User as DBUser
from app.models.user import User, UserCreate, UserInDB, UserUpdate, UserOpen
from app.utils import send_subscription_success_email, send_new_account_email
from app.models.item import Item, ItemCreate

router = APIRouter()


@router.get("/", response_model=List[User])
def read_users(
    db: Session = Depends(get_db),
    skip: int = 0,
    limit: int = 300,
    current_user: DBUser = Depends(get_current_active_superuser),
):
    """
    Retrieve users.
    """
    users = user_crud.get_multi(db, skip=skip, limit=limit)
    return users


@router.post("/", response_model=User)
def create_user(
    *,
    db: Session = Depends(get_db),
    user_in: UserCreate,
current_user: DBUser = Depends(get_current_active_superuser),
):
    """
    Create new user.
    """
    user = user_crud.get_by_email(db, email=user_in.email)
    if user:
        raise HTTPException(
            status_code=400,
            detail="The user with this username already exists in the system.",
        )
    user = user_crud.create(db
, user_in=user_in)

    if config.EMAILS_ENABLED and user_in.email:
        send_new_account_email(
            email_to=user_in.email, username=user_in.email, password=user_in.password
        )
    return user


@router.get("/services", response_model=List[Item])
def read_user_services(
    *,
    db: Session = Depends(get_db),
    user_id: int = None,
    skip: int = 0,
    limit: int = 300,
    current_user: DBUser = Depends(get_current_active_user),
):
    """
    Retrieve user's services.
    """

    if not user_id:
        user_id = current_user.id
    if not user_crud.is_superuser(current_user) and (user_id != current_user.id):
        raise HTTPException(status_code=400, detail="Not enough permissions")
    
    items = item_crud.get_multi_by_owner(db_session=db, owner_id=user_id, skip=skip, limit=limit)

    return items


@router.post("/services", response_model=Item)
def create_user_service(
    *,
    db: Session = Depends(get_db),
    item_in: ItemCreate,
    verify_token: str = None,
    current_user: DBUser = Depends(get_current_active_user),
):
    """
    Added services to current user.
    """
    item = item_crud.create(db_session=db, item_in=item_in, owner_id=current_user.id, created_by=current_user.email)
    if config.EMAILS_ENABLED and current_user.email:
        send_subscription_success_email(
            email_to=current_user.email, username=current_user.email, service=item_in.title
        )
    return item


@router.put("/me", response_model=User)
def update_user_me(
    *,
    db: Session = Depends(get_db),
    password: str = Body(None),
    first_name: str = Body(None),
    last_name: str = Body(None),
    gender: int = Body(None),
    birthday: Optional[datetime] = Body(None),
    company_name: str = Body(None),
    position: str = Body(None),
    country_name: str = Body(None),
    picture_id: int = Body(None),
    email: EmailStr = Body(None),
    current_user: DBUser = Depends(get_current_active_user),
):
    """
    Update own user.
    """
    current_user_data = jsonable_encoder(current_user)
    user_in = UserUpdate(**current_user_data)
    if password is not None:
        user_in.password = password
    if first_name is not None:
        user_in.first_name = first_name
        user_in.full_name = first_name + " "
    if last_name is not None:
        user_in.last_name = last_name
        user_in.full_name = user_in.full_name + last_name
    if gender is not None:
        user_in.gender = gender
    if birthday is not None:
        user_in.birthday = birthday
    if company_name is not None:
        user_in.company_name = company_name
    if position is not None:
        user_in.position = position
    if country_name is not None:
        user_in.country_name = country_name
    if picture_id is not None:
        user_in.picture_id = picture_id
    if email is not None:
        user_in.email = email
    user = user_crud.update(db, user=current_user, user_in=user_in)
    return user


@router.get("/me", response_model=User)
def read_user_me(
    db: Session = Depends(get_db),
    current_user: DBUser = Depends(get_current_active_user),
):
    """
    Get current user.
    """
    return current_user


@router.get("/open", response_model=List[UserOpen])
def read_users_open(
    db: Session = Depends(get_db),
    skip: int = 0,
    limit: int = 300,
    current_user: DBUser = Depends(get_current_active_user),
):
    """
    Retrieve users open.
    """
    users = user_crud.get_multi(db, skip=skip, limit=limit)
    return users


@router.post("/open", response_model=User)
def create_user_open(
    *,
    db: Session = Depends(get_db),
    password: str = Body(...),
    email: EmailStr = Body(...),
    first_name: str = Body(...),
    last_name: str = Body(...)
):
    """
    Create new user without the need to be logged in.
    """
    if not config.USERS_OPEN_REGISTRATION:
        raise HTTPException(
            status_code=403,
            detail="Open user resgistration is forbidden on this server",
        )
    user = user_crud.get_by_email(db, email=email)
    if user:
        raise HTTPException(
            status_code=400,
            detail="The user with this username already exists in the system",
        )

    full_name = (first_name + " " + last_name)
    user_in = UserCreate(
            password=password,
            email=email,
            first_name = first_name, 
            last_name =  last_name, 
            full_name = full_name
        )

    user = user_crud.create(db, user_in=user_in)
    if config.EMAILS_ENABLED and user_in.email:
        send_new_account_email(
            email_to=user_in.email, username=user_in.email, password=user_in.password
        )
    return user


@router.get("/{user_id}", response_model=User)
def read_user_by_id(
    user_id: int,
    current_user: DBUser = Depends(get_current_active_user),
    db: Session = Depends(get_db),
):
    """
    Get a specific user by id.
    """
    user = user_crud.get(db, user_id=user_id)
    if user == current_user:
        return user
    if not user_crud.is_superuser(current_user):
        raise HTTPException(
            status_code=400, detail="The user doesn't have enough privileges"
        )
    return user


@router.put("/{user_id}", response_model=User)
def update_user(
    *,
    db: Session = Depends(get_db),
    user_id: int,
    user_in: UserUpdate,
    current_user: UserInDB = Depends(get_current_active_superuser),
):
    """
    Update a user.
    """
    user = user_crud.get(db, user_id=user_id)
    if not user:
        raise HTTPException(
            status_code=404,
            detail="The user with this username does not exist in the system",
        )
    user = user_crud.update(db, user=user, user_in=user_in)
    return user
