from fastapi import Depends, HTTPException
from sqlalchemy.orm import Session
from starlette.status import HTTP_403_FORBIDDEN

from app.crud import user_crud
from app.core import config
from app.db_models.user import User


# def get_current_user(
#     db: Session = Depends(get_db), token: str = Security(reusable_oauth2)
# ):
#     try:
#         payload = jwt.decode(token, config.SECRET_KEY, algorithms=[ALGORITHM])
#         token_data = TokenPayload(**payload)
#     except PyJWTError:
#         raise HTTPException(
#             status_code=HTTP_403_FORBIDDEN, detail="Could not validate credentials"
#         )
#     user = user_crud.get(db, user_id=token_data.user_id)
#     if not user:
#         raise HTTPException(status_code=404, detail="User not found")
#     return user
