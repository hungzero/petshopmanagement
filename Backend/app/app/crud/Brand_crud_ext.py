# Python class implement basic crud for each entity
from typing import List, Optional
from datetime import datetime
from decimal import Decimal
from fastapi.encoders import jsonable_encoder
from sqlalchemy.orm import Session, joinedload, raiseload

from app.db_models.Brand import Brand
from app.models.default.Brand import BrandCreate, BrandUpdate


def get_multi_by_name(db_session: Session, *, name: str, skip=0, limit=100) -> List[Optional[Brand]]:
    return db_session.query(Brand)\
            .filter(Brand.is_deleted == False)\
            .filter(Brand.name.like("%"+name+"%"))\
            .offset(skip).limit(limit).all()
