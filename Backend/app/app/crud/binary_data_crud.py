# Python class implement basic crud for each entity
from typing import List, Optional
from datetime import datetime
from decimal import Decimal
from fastapi.encoders import jsonable_encoder
from sqlalchemy.orm import Session, joinedload, raiseload

from app.crud.utils import query_helper
from app.db_models.BinaryData import BinaryData
from app.models.binary_data import BinaryDataCreate, BinaryDataUpdate


def get(db_session: Session, *, id: int) -> Optional[BinaryData]:
    return db_session.query(BinaryData).options(joinedload("*")).filter(BinaryData.id == id).first()


def get_by_uuid(db_session: Session, *, uuid: str) -> Optional[BinaryData]:
    return db_session.query(BinaryData).filter(BinaryData.uuid == uuid).first()


def get_multi(db_session: Session, *, skip=0, limit=100) -> List[Optional[BinaryData]]:
    return db_session.query(BinaryData)\
            .offset(skip).limit(limit).all()


def get_multi_by_filter(db_session: Session, *, skip=0, limit=100, load_relations: str = None, has_total: bool = False, filter: List[str] = None) -> List[Optional[BinaryData]]:
    my_query = db_session.query(BinaryData)

    if load_relations:
        my_query = query_helper.option_load_relation_query(BinaryData,my_query, load_relations)

    if filter:
        my_query = query_helper.filter_query(BinaryData, my_query, filter)


    total_count = None
    if has_total:
        total_count = my_query.count()

    results = my_query.offset(skip).limit(limit).all()

    return {"total_count" : total_count,"results" : results}


def create(db_session: Session, *, 
            binarydata_in,
            created_by: str = "") -> BinaryData:

    binarydata = BinaryData(**binarydata_in)

    db_session.add(binarydata)
    db_session.commit()
    db_session.refresh(binarydata)
    return binarydata


def update(db_session: Session, *, 
            binarydata: BinaryData,
            binarydata_in: BinaryDataUpdate,
            updated_by: str = "") -> BinaryData:

    binarydata_in.time_stamp = (binarydata_in.time_stamp.strftime("%Y-%m-%d %H:%M:%S") if binarydata_in.time_stamp else None)

    binarydata_data = jsonable_encoder(binarydata)
    update_data = binarydata_in.dict(skip_defaults=True)
    for field in binarydata_data:
        if field in update_data:
            setattr(binarydata, field, update_data[field])

    db_session.add(binarydata)
    db_session.commit()
    db_session.refresh(binarydata)
    return binarydata


def delete(db_session: Session, *, id: int):
    binarydata = db_session.query(BinaryData).filter(BinaryData.id == id).first()
    db_session.delete(binarydata)
    db_session.commit()
    return binarydata