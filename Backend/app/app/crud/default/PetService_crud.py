# Python class implement basic crud for each entity
from typing import List, Optional
from datetime import datetime
from decimal import Decimal
from fastapi.encoders import jsonable_encoder
from sqlalchemy.orm import Session, joinedload, raiseload

from app.crud.utils import query_helper
from app.db_models.PetService import PetService
from app.models.default.PetService import PetServiceCreate, PetServiceUpdate


def get(db_session: Session, *, id: int) -> Optional[PetService]:
    return db_session.query(PetService).options(joinedload("*")).filter(PetService.id == id).first()


def get_multi(db_session: Session, *, skip=0, limit=100) -> List[Optional[PetService]]:
    return db_session.query(PetService)\
            .offset(skip).limit(limit).all()


def get_multi_by_filter(db_session: Session, *, skip=0, limit=100, load_relations: str = None, has_total: bool = False, filter: List[str] = None) -> List[Optional[PetService]]:
    my_query = db_session.query(PetService)

    if load_relations:
        my_query = query_helper.option_load_relation_query(PetService,my_query, load_relations)

    if filter:
        my_query = query_helper.filter_query(PetService, my_query, filter)


    total_count = None
    if has_total:
        total_count = my_query.count()

    results = my_query.offset(skip).limit(limit).all()

    return {"total_count" : total_count,"results" : results}



def create(db_session: Session, *, 
            petservice_in: PetServiceCreate,
            created_by: str = "") -> PetService:

    petservice_in.date = (petservice_in.date.strftime("%Y-%m-%d %H:%M:%S") if petservice_in.date else None)

    petservice_in_data = jsonable_encoder(petservice_in)
    petservice = PetService(**petservice_in_data)


    db_session.add(petservice)
    db_session.commit()
    db_session.refresh(petservice)
    return petservice


def update(db_session: Session, *, 
            petservice: PetService,
            petservice_in: PetServiceUpdate,
            updated_by: str = "") -> PetService:

    petservice_in.date = (petservice_in.date.strftime("%Y-%m-%d %H:%M:%S") if petservice_in.date else None)

    petservice_data = jsonable_encoder(petservice)
    update_data = petservice_in.dict(skip_defaults=True)
    for field in petservice_data:
        if field in update_data:
            setattr(petservice, field, update_data[field])

    db_session.add(petservice)
    db_session.commit()
    db_session.refresh(petservice)
    return petservice




def delete(db_session: Session, *, id: int):
    petservice = db_session.query(PetService).filter(PetService.id == id).first()
    db_session.delete(petservice)
    db_session.commit()
    return petservice