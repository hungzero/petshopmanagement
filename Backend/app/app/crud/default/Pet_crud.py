# Python class implement basic crud for each entity
from typing import List, Optional
from datetime import datetime
from decimal import Decimal
from fastapi.encoders import jsonable_encoder
from sqlalchemy.orm import Session, joinedload, raiseload

from app.crud.utils import query_helper
from app.db_models.Pet import Pet
from app.models.default.Pet import PetCreate, PetUpdate


def get(db_session: Session, *, id: int) -> Optional[Pet]:
    return db_session.query(Pet).options(joinedload("*")).filter(Pet.id == id).first()


def get_multi(db_session: Session, *, skip=0, limit=100) -> List[Optional[Pet]]:
    return db_session.query(Pet)\
            .offset(skip).limit(limit).all()


def get_multi_by_filter(db_session: Session, *, skip=0, limit=100, load_relations: str = None, has_total: bool = False, filter: List[str] = None) -> List[Optional[Pet]]:
    my_query = db_session.query(Pet)

    if load_relations:
        my_query = query_helper.option_load_relation_query(Pet,my_query, load_relations)

    if filter:
        my_query = query_helper.filter_query(Pet, my_query, filter)


    total_count = None
    if has_total:
        total_count = my_query.count()

    results = my_query.offset(skip).limit(limit).all()

    return {"total_count" : total_count,"results" : results}



def create(db_session: Session, *, 
            pet_in: PetCreate,
            created_by: str = "") -> Pet:


    pet_in_data = jsonable_encoder(pet_in)
    pet = Pet(**pet_in_data)


    db_session.add(pet)
    db_session.commit()
    db_session.refresh(pet)
    return pet


def update(db_session: Session, *, 
            pet: Pet,
            pet_in: PetUpdate,
            updated_by: str = "") -> Pet:


    pet_data = jsonable_encoder(pet)
    update_data = pet_in.dict(skip_defaults=True)
    for field in pet_data:
        if field in update_data:
            setattr(pet, field, update_data[field])

    db_session.add(pet)
    db_session.commit()
    db_session.refresh(pet)
    return pet




def delete(db_session: Session, *, id: int):
    pet = db_session.query(Pet).filter(Pet.id == id).first()
    db_session.delete(pet)
    db_session.commit()
    return pet