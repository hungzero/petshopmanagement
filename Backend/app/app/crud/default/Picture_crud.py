# Python class implement basic crud for each entity
from typing import List, Optional
from datetime import datetime
from decimal import Decimal
from fastapi.encoders import jsonable_encoder
from sqlalchemy.orm import Session, joinedload, raiseload

from app.crud.utils import query_helper
from app.db_models.Picture import Picture
from app.models.default.Picture import PictureCreate, PictureUpdate


def get(db_session: Session, *, id: int) -> Optional[Picture]:
    return db_session.query(Picture).options(joinedload("*")).filter(Picture.id == id).first()


def get_multi(db_session: Session, *, skip=0, limit=100) -> List[Optional[Picture]]:
    return db_session.query(Picture)\
            .filter(Picture.is_deleted == False)\
            .offset(skip).limit(limit).all()


def get_multi_by_filter(db_session: Session, *, skip=0, limit=100, load_relations: str = None, has_total: bool = False, filter: List[str] = None) -> List[Optional[Picture]]:
    my_query = db_session.query(Picture)

    if load_relations:
        my_query = query_helper.option_load_relation_query(Picture,my_query, load_relations)

    if filter:
        my_query = query_helper.filter_query(Picture, my_query, filter)

    my_query.filter(Picture.is_deleted == False)

    total_count = None
    if has_total:
        total_count = my_query.count()

    results = my_query.offset(skip).limit(limit).all()

    return {"total_count" : total_count,"results" : results}



def create(db_session: Session, *, 
            picture_in: PictureCreate,
            created_by: str = "") -> Picture:


    picture_in_data = jsonable_encoder(picture_in)
    picture = Picture(**picture_in_data)

    picture.is_deleted = False
    picture.created_by = created_by
    picture.created_date = datetime.now().strftime("%Y-%m-%d %H:%M:%S")

    db_session.add(picture)
    db_session.commit()
    db_session.refresh(picture)
    return picture


def update(db_session: Session, *, 
            picture: Picture,
            picture_in: PictureUpdate,
            updated_by: str = "") -> Picture:


    picture_data = jsonable_encoder(picture)
    update_data = picture_in.dict(skip_defaults=True)
    for field in picture_data:
        if field in update_data:
            setattr(picture, field, update_data[field])

    picture.updated_by = updated_by
    picture.updated_date = datetime.now().strftime("%Y-%m-%d %H:%M:%S")
    db_session.add(picture)
    db_session.commit()
    db_session.refresh(picture)
    return picture


def delete_soft(db_session: Session, *, 
            id: int,
            deleted_by: str = "") -> Picture:
    update_data = db_session.query(Picture).filter(Picture.id == id).first()
    if update_data:
        update_data.is_deleted = True
        update_data.deleted_by = deleted_by
        update_data.deleted_date = datetime.now().strftime("%Y-%m-%d %H:%M:%S")
        db_session.add(update_data)
        db_session.commit()
        db_session.refresh(update_data)  
    return update_data


def delete(db_session: Session, *, id: int):
    picture = db_session.query(Picture).filter(Picture.id == id).first()
    db_session.delete(picture)
    db_session.commit()
    return picture