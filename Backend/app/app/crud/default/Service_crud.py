# Python class implement basic crud for each entity
from typing import List, Optional
from datetime import datetime
from decimal import Decimal
from fastapi.encoders import jsonable_encoder
from sqlalchemy.orm import Session, joinedload, raiseload

from app.crud.utils import query_helper
from app.db_models.Service import Service
from app.models.default.Service import ServiceCreate, ServiceUpdate


def get(db_session: Session, *, id: int) -> Optional[Service]:
    return db_session.query(Service).options(joinedload("*")).filter(Service.id == id).first()


def get_multi(db_session: Session, *, skip=0, limit=100) -> List[Optional[Service]]:
    return db_session.query(Service)\
            .offset(skip).limit(limit).all()


def get_multi_by_filter(db_session: Session, *, skip=0, limit=100, load_relations: str = None, has_total: bool = False, filter: List[str] = None) -> List[Optional[Service]]:
    my_query = db_session.query(Service)

    if load_relations:
        my_query = query_helper.option_load_relation_query(Service,my_query, load_relations)

    if filter:
        my_query = query_helper.filter_query(Service, my_query, filter)


    total_count = None
    if has_total:
        total_count = my_query.count()

    results = my_query.offset(skip).limit(limit).all()

    return {"total_count" : total_count,"results" : results}



def create(db_session: Session, *, 
            service_in: ServiceCreate,
            created_by: str = "") -> Service:


    service_in_data = jsonable_encoder(service_in)
    service = Service(**service_in_data)


    db_session.add(service)
    db_session.commit()
    db_session.refresh(service)
    return service


def update(db_session: Session, *, 
            service: Service,
            service_in: ServiceUpdate,
            updated_by: str = "") -> Service:


    service_data = jsonable_encoder(service)
    update_data = service_in.dict(skip_defaults=True)
    for field in service_data:
        if field in update_data:
            setattr(service, field, update_data[field])

    db_session.add(service)
    db_session.commit()
    db_session.refresh(service)
    return service




def delete(db_session: Session, *, id: int):
    service = db_session.query(Service).filter(Service.id == id).first()
    db_session.delete(service)
    db_session.commit()
    return service