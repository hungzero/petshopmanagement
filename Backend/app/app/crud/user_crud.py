from typing import List, Optional

from fastapi.encoders import jsonable_encoder
from sqlalchemy.orm import Session

from app.core.security import get_password_hash, verify_password
from app.db_models.user import User
from app.models.user import UserCreate, UserUpdate
from datetime import datetime


def get(db_session: Session, *, user_id: int) -> Optional[User]:
    return db_session.query(User).filter(User.id == user_id).first()


def get_by_email(db_session: Session, *, email: str) -> Optional[User]:
    return db_session.query(User).filter(User.email == email).first()


def authenticate(db_session: Session, *, email: str, password: str) -> Optional[User]:
    user = get_by_email(db_session, email=email)
    if not user:
        return None
    if not verify_password(password, user.hashed_password):
        return None
    return user


def is_active(user) -> bool:
    return user.is_active


def is_superuser(user) -> bool:
    return user.is_superuser


def get_multi(db_session: Session, *, skip=0, limit=100) -> List[Optional[User]]:
    return db_session.query(User).offset(skip).limit(limit).all()


def create(db_session: Session, *, user_in: UserCreate) -> User:
    user = User(
        email=user_in.email,
        hashed_password=get_password_hash(user_in.password),
        full_name = user_in.full_name,
        first_name=user_in.first_name,
        last_name=user_in.last_name,
        birthday = (user_in.birthday.strftime("%Y-%m-%d %H:%M:%S") if user_in.birthday else None),
        company_name = user_in.company_name,
        position = user_in.position,
        country_name = user_in.country_name,
        is_superuser=user_in.is_superuser,
        is_active = True,
        created_by = "",
        created_date = datetime.now().strftime("%Y-%m-%d %H:%M:%S"),
        is_deleted = False
    )

    db_session.add(user)
    db_session.commit()
    db_session.refresh(user)
    return user


def update(db_session: Session, *, user: User, user_in: UserUpdate) -> User:

    user.birthday = (user.birthday.strftime("%Y-%m-%d %H:%M:%S") if user.birthday else None)

    user_data = jsonable_encoder(user)
    update_data = user_in.dict(skip_defaults=True)
    for field in user_data:
        if field in update_data:
            setattr(user, field, update_data[field])
    if user_in.password:
        passwordhash = get_password_hash(user_in.password)
        user.hashed_password = passwordhash
    
    user.updated_by = "",
    user.updated_date = datetime.now().strftime("%Y-%m-%d %H:%M:%S"),

    db_session.add(user)
    db_session.commit()
    db_session.refresh(user)
    return user
