
from typing import List, Optional
from datetime import datetime
from decimal import Decimal
from sqlalchemy.orm import Session, joinedload, raiseload

def filter_query(model_class, query, filter_condition):
    '''
    Return filtered queryset based on condition.
    :param model_class: query's model class
    :param query: takes query
    :param filter_condition: Its a list of string, ie: ["age;in;5,6,7;desc","name;like;%john%;asc"]
    operator list:
        eq for ==
        lt for <
        ge for >=
        in for in_
        like for like
    value could be list or a string
    sort can be 'desc' or 'asc' or '' for not sort
    :return: queryset
    '''
    for raw in filter_condition:
        try:
            (key, op, value, sort) = raw.split(";", 4)
        except ValueError:
            raise ValueError('Invalid filter: %s' % raw)
        column = getattr(model_class, key, None)

        if not column:
            raise ValueError('Invalid filter column: %s' % key)

        if op:
            if op == 'in':
                if isinstance(value, list):
                    filt = column.in_(value)
                else:
                    filt = column.in_(value.split(','))
            else:
                try:
                    attr = list(filter(
                        lambda e: hasattr(column, e % op),
                        ['%s', '%s_', '__%s__']
                    ))[0] % op
                except IndexError:
                    raise ValueError('Invalid filter operator: %s' % op)
                if value.lower() == 'null':
                    value = None
                filt = getattr(column, attr)(value)
                
            query = query.filter(filt)

        if sort:
            if sort.lower() == "desc":
                query = query.order_by(column.desc())
            elif sort.lower() == "asc":
                query = query.order_by(column.asc())
            else:
                raise ValueError('Invalid filter sort opeartor: %s' % sort)

    return query


def option_load_relation_query(model_class, query, load_relation_list):
    '''
    Return query that has option to load relation entities.
    :param model_class: query's model class
    :param query: takes query
    :param load_relation_list: list of relation entities to load, separate by comma ex: table1, table2
    :return: queryset
    '''
    try:
        keys = load_relation_list.split(",")
    except ValueError:
        raise ValueError('Invalid relation list: %s' % keys)

    for key in keys:
        column = getattr(model_class, key, None)

        if not column:
            raise ValueError('Invalid relation list column: %s' % key)

        query = query.options(joinedload(key))

    return query


def call_procedure(session, proc_name, params):
    sql_params = ",".join([":{0}".format(name) for name, value in params.items()])
    sql_string = """
        CALL {proc_name}({params})
    """.format(proc_name=proc_name, params=sql_params)

    return session.execute(sql_string, params).fetchall()
