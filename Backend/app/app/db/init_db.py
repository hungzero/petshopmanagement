from app.crud import user_crud
from app.core import config
from app.models.user import UserCreate
from datetime import datetime

# make sure all SQL Alchemy models are imported before initializing DB
# otherwise, SQL Alchemy might fail to initialize properly relationships
# for more details: https://github.com/tiangolo/full-stack-fastapi-postgresql/issues/28
from app.db import base
from app.db.session import db_session, engine


def init_db(db_session):
    # Tables should be created with Alembic migrations
    # But if you don't want to use migrations, create
    # the tables un-commenting the next line
    base.Base.metadata.create_all(bind=engine)

    user = user_crud.get_by_email(db_session, email=config.FIRST_SUPERUSER)
    if not user:
        user_in = UserCreate(
            email=config.FIRST_SUPERUSER,
            full_name = config.FIRST_SUPERUSER,
            first_name = config.FIRST_SUPERUSER,
            last_name = config.FIRST_SUPERUSER,
            gender=0,
            password=config.FIRST_SUPERUSER_PASSWORD,
            is_superuser=True,
            birthday=datetime.now(),
            picture_id=1,
            is_active=True
        )

        user = user_crud.create(db_session, user_in=user_in)