# Python class represent the database tables
from sqlalchemy import Column, ForeignKey, Integer, String, Boolean, DateTime, DECIMAL, BLOB
from sqlalchemy.orm import relationship

from app.db.base_class import Base


class BinaryData(Base):
    __tablename__ = "BinaryData"
    
    id = Column(Integer, primary_key=True, index=True, nullable=False)
    uuid = Column(String(36), nullable=False)
    description = Column(String(128), nullable=True)
    bin_data = Column(BLOB, nullable=True)
    file_name = Column(String(128), nullable=True)
    file_size = Column(String(30), nullable=True)
    file_type = Column(String(30), nullable=True)
    time_stamp = Column(DateTime, nullable=True)

