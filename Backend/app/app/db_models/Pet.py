# Python class represent the database tables
from sqlalchemy import Column, ForeignKey, Integer, String, Boolean, DateTime, DECIMAL
from sqlalchemy.orm import relationship

from app.db.base_class import Base


class Pet(Base):
    __tablename__ = "Pet"
    
    id = Column(Integer, primary_key=True, index=True, nullable=False)
    name = Column(String(50), nullable=False)
    specie = Column(String(255), nullable=False)
    age = Column(Integer, nullable=True)
    gender = Column(String(255), nullable=True)
    health = Column(String(255), nullable=True)
    color = Column(String(255), nullable=True)
    picture_id = Column(Integer, ForeignKey("Picture.id"), nullable=False )

    picture = relationship("Picture", lazy='noload', foreign_keys=[picture_id])
