# Python class represent the database tables
from sqlalchemy import Column, ForeignKey, Integer, String, Boolean, DateTime, DECIMAL
from sqlalchemy.orm import relationship

from app.db.base_class import Base


class PetService(Base):
    __tablename__ = "PetService"
    
    id = Column(Integer, primary_key=True, index=True, nullable=False)
    pet_id = Column(Integer, ForeignKey("Pet.id"), nullable=False )
    service_id = Column(Integer, ForeignKey("Service.id"), nullable=False )
    date = Column(DateTime, nullable=True)
    bill_number = Column(String(100), nullable=True)

    pet = relationship("Pet", lazy='noload', foreign_keys=[pet_id])
    service = relationship("Service", lazy='noload', foreign_keys=[service_id])
