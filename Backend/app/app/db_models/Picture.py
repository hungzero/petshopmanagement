# Python class represent the database tables
from sqlalchemy import Column, ForeignKey, Integer, String, Boolean, DateTime, DECIMAL
from sqlalchemy.orm import relationship

from app.db.base_class import Base


class Picture(Base):
    __tablename__ = "Picture"
    
    id = Column(Integer, primary_key=True, index=True, nullable=False)
    name = Column(String(255), nullable=False)
    type = Column(String(255), nullable=False)
    url = Column(String(255), nullable=False)
    seo_file_name = Column(String(255), nullable=True)
    is_deleted = Column(Boolean, nullable=False)
    created_by = Column(String(255), nullable=False)
    created_date = Column(DateTime, nullable=False)
    updated_by = Column(String(255), nullable=True)
    updated_date = Column(DateTime, nullable=True)
    deleted_by = Column(String(255), nullable=True)
    deleted_date = Column(DateTime, nullable=True)

