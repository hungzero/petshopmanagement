# Python class represent the database tables
from sqlalchemy import Column, ForeignKey, Integer, String, Boolean, DateTime, DECIMAL
from sqlalchemy.orm import relationship

from app.db.base_class import Base


class Service(Base):
    __tablename__ = "Service"
    
    id = Column(Integer, primary_key=True, index=True, nullable=False)
    name = Column(String(50), nullable=False)
    code = Column(String(255), nullable=False)
    price = Column(DECIMAL(18,6), nullable=False)

