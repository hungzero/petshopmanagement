from sqlalchemy import Boolean, Column, Integer, String, DateTime, ForeignKey
from sqlalchemy.orm import relationship

from app.db.base_class import Base


class Item(Base):
    __tablename__ = "Item"
    
    id = Column(Integer, primary_key=True, index=True)
    title = Column(String(128), index=True)
    description = Column(String(256))
    owner_id = Column(Integer, ForeignKey("User.id"))

    is_deleted = Column(Boolean, nullable=False, default=False)
    created_by = Column(String(255), nullable=False)
    created_date = Column(DateTime, nullable=False)
    updated_by = Column(String(255), nullable=True)
    updated_date = Column(DateTime, nullable=True)
    deleted_by = Column(String(255), nullable=True)
    deleted_date = Column(DateTime, nullable=True)

    owner = relationship("User", foreign_keys=[owner_id])
