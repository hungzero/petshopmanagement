from sqlalchemy import Boolean, Column, Integer, String, DateTime, ForeignKey
from sqlalchemy.orm import relationship

from app.db.base_class import Base


class User(Base):
    __tablename__ = "User"

    id = Column(Integer, primary_key=True, index=True)    
    email = Column(String(256), unique=True, index=True)
    full_name = Column(String(256))
    first_name = Column(String(128))
    last_name = Column(String(128))
    gender = Column(Integer, default = 0)
    hashed_password = Column(String(512))
    birthday = Column(DateTime, nullable=True)
    company_name = Column(String(255), nullable=True)
    position = Column(String(255), nullable=True)
    country_name = Column(String(255), nullable=True)
    picture_id = Column(Integer, ForeignKey("Picture.id"), nullable=True)
    is_active = Column(Boolean(), default=True)
    is_superuser = Column(Boolean(), default=False)
    
    is_deleted = Column(Boolean, nullable=False, default=False)
    created_by = Column(String(255), nullable=False)
    created_date = Column(DateTime, nullable=False)
    updated_by = Column(String(255), nullable=True)
    updated_date = Column(DateTime, nullable=True)
    deleted_by = Column(String(255), nullable=True)
    deleted_date = Column(DateTime, nullable=True)

    picture = relationship("Picture", foreign_keys=[picture_id]) 
