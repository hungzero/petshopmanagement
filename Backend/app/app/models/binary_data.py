# Python class represent the entities

from typing import Optional
from pydantic import BaseModel
from datetime import datetime
from decimal import Decimal

# Shared properties
class BinaryDataBase(BaseModel):
    uuid: str
    description: Optional[str] = None
    bin_data: Optional[bytes] = None
    file_name: Optional[str] = None
    file_size: Optional[str] = None
    file_type: Optional[str] = None
    time_stamp: Optional[datetime] = None

    class Config:
        orm_mode = True


# Properties to receive on item creation
class BinaryDataCreate(BinaryDataBase):    
    pass


# Properties to receive on item update
class BinaryDataUpdate(BinaryDataCreate):
    pass


# Properties shared by models stored in DB
class BinaryDataInDBBase(BinaryDataBase):
    id: int


# Properties to return to client
class BinaryData(BinaryDataInDBBase):
    pass

# Properties to return to client when in link
class BinaryDataLink(BaseModel):
    id: int
    uuid: str = None
    file_name: str = None
    file_type: str = None
    class Config:
        orm_mode = True