# Python class represent the entities

from typing import Optional
from pydantic import BaseModel
from datetime import datetime
from decimal import Decimal
from app.models.default.Picture import Picture, PictureLink

# Shared properties
class PetBase(BaseModel):
    name: str
    specie: str
    age: Optional[int] = None
    gender: Optional[str] = None
    health: Optional[str] = None
    color: Optional[str] = None
    picture_id: int

    class Config:
        orm_mode = True


# Properties to receive on item creation
class PetCreate(PetBase):    
    pass


# Properties to receive on item update
class PetUpdate(PetCreate):
    pass


# Properties shared by models stored in DB
class PetInDBBase(PetBase):
    id: int


# Properties to return to client
class Pet(PetInDBBase):
    picture: PictureLink = None
    pass

# Properties to return to client when in link
class PetLink(BaseModel):
    id: int
    name: str = None
    picture_id: int = None
    class Config:
        orm_mode = True