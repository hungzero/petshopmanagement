# Python class represent the entities

from typing import Optional
from pydantic import BaseModel
from datetime import datetime
from decimal import Decimal
from app.models.default.Pet import Pet, PetLink
from app.models.default.Service import Service, ServiceLink

# Shared properties
class PetServiceBase(BaseModel):
    pet_id: int
    service_id: int
    date: Optional[datetime] = None
    bill_number: Optional[str] = None

    class Config:
        orm_mode = True


# Properties to receive on item creation
class PetServiceCreate(PetServiceBase):    
    pass


# Properties to receive on item update
class PetServiceUpdate(PetServiceCreate):
    pass


# Properties shared by models stored in DB
class PetServiceInDBBase(PetServiceBase):
    id: int


# Properties to return to client
class PetService(PetServiceInDBBase):
    pet: PetLink = None
    service: ServiceLink = None
    pass

# Properties to return to client when in link
class PetServiceLink(BaseModel):
    id: int
    pet_id: int = None
    service_id: int = None
    class Config:
        orm_mode = True