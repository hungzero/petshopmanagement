# Python class represent the entities

from typing import Optional
from pydantic import BaseModel
from datetime import datetime
from decimal import Decimal

# Shared properties
class PictureBase(BaseModel):
    name: str
    type: str
    url: str
    seo_file_name: Optional[str] = None

    class Config:
        orm_mode = True


# Properties to receive on item creation
class PictureCreate(PictureBase):    
    pass


# Properties to receive on item update
class PictureUpdate(PictureCreate):
    pass


# Properties shared by models stored in DB
class PictureInDBBase(PictureBase):
    id: int


# Properties to return to client
class Picture(PictureInDBBase):
    pass

# Properties to return to client when in link
class PictureLink(BaseModel):
    id: int
    name: str = None
    type: str = None
    url: str = None
    seo_file_name: str = None
    class Config:
        orm_mode = True