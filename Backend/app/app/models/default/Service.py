# Python class represent the entities

from typing import Optional
from pydantic import BaseModel
from datetime import datetime
from decimal import Decimal

# Shared properties
class ServiceBase(BaseModel):
    name: str
    code: str
    price: Decimal

    class Config:
        orm_mode = True


# Properties to receive on item creation
class ServiceCreate(ServiceBase):    
    pass


# Properties to receive on item update
class ServiceUpdate(ServiceCreate):
    pass


# Properties shared by models stored in DB
class ServiceInDBBase(ServiceBase):
    id: int


# Properties to return to client
class Service(ServiceInDBBase):
    pass

# Properties to return to client when in link
class ServiceLink(BaseModel):
    id: int
    name: str = None
    code: str = None
    price: Decimal = None
    class Config:
        orm_mode = True