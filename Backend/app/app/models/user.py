from typing import Optional
from app.models.default.Picture import Picture
from pydantic import BaseModel
from datetime import datetime

# Shared properties
class UserBase(BaseModel):
    email: Optional[str] = None
    full_name: Optional[str] = None
    first_name: Optional[str] = None
    last_name: Optional[str] = None
    gender: Optional[int] = 0
    birthday: Optional[datetime] = None
    company_name: Optional[str] = None
    position: Optional[str] = None
    country_name: Optional[str] = None
    picture_id: Optional[int] = None
    is_active: Optional[bool] = True
    is_superuser: Optional[bool] = False

    class Config:
        orm_mode = True


class UserBaseInDB(UserBase):
    id: int = None


# Properties to receive via API on creation
class UserCreate(UserBaseInDB):
    email: str
    password: str


# Properties to receive via API on update
class UserUpdate(UserBaseInDB):
    password: Optional[str] = None


# Additional properties to return via API
class User(UserBaseInDB):
    picture: Optional[Picture] = None
    pass


# Additional properties stored in DB
class UserInDB(UserBaseInDB):
    hashed_password: str


# Open version of user to return from open call
class UserOpen(BaseModel):
    id: int = None
    email: Optional[str] = None
    full_name: Optional[str] = None
    picture_id: Optional[int] = None
    picture: Optional[Picture] = None
    class Config:
        orm_mode = True
