#! /bin/sh

echo "Start run prestart content..."

# Let the DB start
python /app/backend_pre_start.py

# Run migrations
# alembic revision --autogenerate -m "add some more tables"
# alembic upgrade head

# Create initial data in DB
python /app/initial_data.py

"$@"