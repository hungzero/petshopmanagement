SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";

--
-- Database: `monkey_market`
--

--
-- Dumping data for table `User`
--

REPLACE INTO `User` (`id`, `email`, `full_name`, `first_name`, `last_name`, `gender`, `hashed_password`, `birthday`, `country_name`, `company_name`, `position`, `picture_id`, `is_active`, `is_superuser`, `is_deleted`, `created_by`, `created_date`, `updated_by`, `updated_date`, `deleted_by`, `deleted_date`) VALUES
(1, 'admin@monkeymarket.com', 'admin@monkeymarket.com', 'admin@monkeymarket.com', 'admin@monkeymarket.com', 0, '$2b$12$lRSMkjYwxQeNB58lK7ImcexWUfhARUjQrsww8ut3lsHUzB6PjpKYS', '2019-12-26 05:24:36','Vietnam','Sichuan pte Ltd','CEO', 1, 1, 1, 0, '', '2019-12-26 05:24:36', NULL, NULL, NULL, NULL);
COMMIT;

